package com.company.graph2;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> distances = new HashMap<>();
        ArrayList<Integer> picked = new ArrayList<>();
        int max = 1000000000;
        for(int i = 0; i < adjacencyMatrix.length; i++){
            for(int j = 0; j < adjacencyMatrix.length; j++){
                if(adjacencyMatrix[i][j] == 0){
                    if(i != j) {
                        adjacencyMatrix[i][j] = max;
                        adjacencyMatrix[j][i] = max;
                    }
                }
            }
            distances.put(i, adjacencyMatrix[startIndex][i]);
        }

        int pick = startIndex;
        while(picked.size()<adjacencyMatrix.length) {
            int m = 1000000000;
            picked.add(pick);
            for (int i = 0; i < picked.size(); i++) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (!picked.contains(j)) {
                        distances.put(j, Math.min(distances.get(j), distances.get(picked.get(i)) + adjacencyMatrix[picked.get(i)][j]));
                        if ((distances.get(j) > 0) && (distances.get(j) < m)){
                            m = distances.get(j);
                            pick = j;
                        }
                    }
                }
            }
        }


        return distances;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        ArrayList<Integer> used = new ArrayList<>();
        ArrayList<Integer> notUsed = new ArrayList<>();
        for(int i = 0; i < adjacencyMatrix.length; i++){
            notUsed.add(i);
        }

        int pick = 0;
        int answer = 0;
        int m = 0;
        while(!notUsed.isEmpty()){
            answer = answer + m;
            used.add(pick);
            notUsed.remove(notUsed.indexOf(pick));
            m = 2147483647;
            for(int i = 0; i < used.size(); i++){
                for(int j = 0; j < notUsed.size(); j++)
                    if((adjacencyMatrix[used.get(i)][notUsed.get(j)] != 0) && (adjacencyMatrix[used.get(i)][notUsed.get(j)] <= m)){
                        pick = notUsed.get(j);
                        m = adjacencyMatrix[used.get(i)][notUsed.get(j)];
                    }
            }
        }
        return answer;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        TreeMap<Integer, ArrayList<Pair<Integer, Integer>>> edges = new TreeMap<>();
        for(int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if(adjacencyMatrix[i][j] != 0){
                    if(i < j){
                        Pair<Integer, Integer> p = new Pair(i, j);
                        if(edges.containsKey(adjacencyMatrix[i][j])) {
                            edges.get(adjacencyMatrix[i][j]).add(p);
                        }
                        else{
                            ArrayList<Pair<Integer, Integer>> myPairs = new ArrayList<>();
                            myPairs.add(p);
                            edges.put(adjacencyMatrix[i][j], myPairs);
                        }
                    }
                }
            }
        }


        List<Integer> keys = new ArrayList<Integer>(edges.keySet());

        ArrayList<ArrayList<Integer>> sets = new ArrayList<>();
        ArrayList<Integer> pair = new ArrayList<>();
        pair.add(edges.get(edges.firstKey()).get(0).getKey());
        pair.add(edges.get(edges.firstKey()).get(0).getValue());
        sets.add(pair);

        ArrayList<Integer> setsDist = new ArrayList<>();
        setsDist.add(edges.firstKey());
        for(int j = 0; j < edges.size(); j++){
            for(int i = 0; i < edges.get(keys.get(j)).size(); i++) {
                for(int k = 0; k < sets.size(); k++) {
                    if ((!sets.get(k).contains(edges.get(keys.get(j)).get(i).getKey())) && (!sets.get(k).contains(edges.get(keys.get(j)).get(i).getValue()))) {
                        ArrayList<Integer> newPair = new ArrayList<>();
                        newPair.add(edges.get(keys.get(j)).get(i).getKey());
                        newPair.add(edges.get(keys.get(j)).get(i).getValue());
                        sets.add(newPair);
                        setsDist.add(keys.get(j));
                        break;

                    }
                    else if((sets.get(k).contains(edges.get(keys.get(j)).get(i).getKey())) && (!sets.get(k).contains(edges.get(keys.get(j)).get(i).getValue()))){
                        sets.get(k).add(edges.get(keys.get(j)).get(i).getValue());
                        setsDist.set(k, setsDist.get(k)+keys.get(j));
                        break;
                    }
                    else if((!sets.get(k).contains(edges.get(keys.get(j)).get(i).getKey())) && (sets.get(k).contains(edges.get(keys.get(j)).get(i).getValue()))){
                        sets.get(k).add(edges.get(keys.get(j)).get(i).getKey());
                        setsDist.set(k, setsDist.get(k)+keys.get(j));
                        break;
                    }
                }
                int total = adjacencyMatrix.length;
                for(int x = 0; x < sets.size(); x++) {
                    for(int y = x+1; y < sets.size(); y++) {
                        for(int z = 0; z < sets.get(y).size(); z++) {
                            if(sets.get(x).contains(sets.get(y).get(z))){
                                sets.get(y).remove(sets.get(y).get(z));
                                sets.get(x).addAll(sets.get(y));
                                sets.remove(sets.get(y));
                                setsDist.set(x, setsDist.get(x)+setsDist.get(y));
                                setsDist.remove(setsDist.get(y));
                                if(sets.get(x).size() == total){
                                    return setsDist.get(x);
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        return setsDist.get(0);
    }
}
